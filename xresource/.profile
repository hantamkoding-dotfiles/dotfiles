export CM_LAUNCHER=rofi
export CM_DIR=~/.cache/clipmenu
export LC_ALL=en_US.UTF-8
export QT_QPA_PLATFORMTHEME=qt5ct
export FONT_NAME="Iosevka 11"

# For Android Studio
export _JAVA_AWT_WM_NONREPARENTING=1

export STUDIO_JDK=/usr/lib/jvm/java-11-openjdk
# For Gradle
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk


# default editor
export EDITOR=nvim
# export GTK_THEME="Matcha-dark-sea"
# export ICON_THEME="Breeze Dark"
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init --path)"
